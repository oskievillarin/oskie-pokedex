## This code challenge is build using Next.js [Learn Next.js](https://nextjs.org/learn).

## Setup Instructions

1. Clone my public repository [https://bitbucket.org/oskievillarin/oskie-pokedex/src/master/](https://bitbucket.org/oskievillarin/oskie-pokedex/src/master/).

2. Type in terminal, "cd oskie-pokedex"

3. Type in terminal, "npm install"

4. Type in terminal, "npm run dev" for Development.

5. Type in terminal, "npm run build" for Build.



**Notes**
- Assuming this is my working Pokemon API url: [https://bitbucket.org/oskievillarin/oskie-pokedex/raw/8f8400bbefd8f98de6a3c4938c6dab632a3a161d/public/pokemon.json](https://bitbucket.org/oskievillarin/oskie-pokedex/raw/8f8400bbefd8f98de6a3c4938c6dab632a3a161d/public/pokemon.json)
- Since i'm using Next.js, so the default entry point is on pages/index.js
- As for the Error & Loading page, I personally not sure or to implement a state page method, I prefer to solve or use a components method on it.

---

## How did you decide on the technical and architectural choices used as part of your solution?

I decided to use Next.js, one of modern Reactjs framework because personally I love Next.js and it's easy to use and develop React applications.

---

## Are there any improvements you could make to your submission?

I think there's a lot that needs improvements on my submission such as implementing React testing using Next.js with Jest and React Testing Library, Improvements on my Data fetching function and applying safely Error handling and implementing loading state.

---

## What would you do differently if you were allocated more time?

I will create testing modules on my Pokedex application, I will improves the UI/UX and make sure to be mobile friendly, improves more on my code which I can make it more useable and less bugs. And lastly if I have more time, I will explore or take a research that is relevant on my application and will implement it that could be more helpful.
