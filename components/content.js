function Content({ pokemonList, state }) {

    if (!state) {
        return (
            <main>
                <div className="container loading">
                    <p>Catching them all...</p>
                </div>
            </main>
        )
    }

    return (
        <main>
          <div className="container grid">
              {
                  pokemonList.map((pokemon) => (
                      <div className="card" key={pokemon.number}>
                          <div className="card-img">
                              <img src={pokemon.image.url} width={pokemon.image.width} height={pokemon.image.height} alt={pokemon.name} />
                          </div>
                          <span className="number">#{pokemon.number}</span>
                          <p className="name">{pokemon.name}</p>
                          <div className="card-action">
                          {
                              pokemon.types.map((type) => (
                                  <span className={`type type-${type}`} key={type}>{type}</span>
                              ))
                          }
                          </div>
                      </div>
                  ))
              }
          </div>
        </main>
    )
}

export default Content
