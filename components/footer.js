export default function Footer({ children }) {
  return (
      <footer>
        <p>Copyright © 2021 React Code Challenge. All Rights Resevered.</p>
        <p>Use of pokemon images performed under fair use. Not for sale or redistribution.</p>
      </footer>
  )
}
