import Head from 'next/head'

export default function Header({ children }) {
  return (
      <div className="header-wrap">
          <Head>
            <title>Pokedex App</title>
            <link rel="icon" href="/favicon.ico" />
          </Head>
          <header className="header">
              <h1 className="title">The Online Pokedex</h1>
          </header>
      </div>
  )
}
