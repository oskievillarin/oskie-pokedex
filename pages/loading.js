import Header from '../components/header'
import Footer from '../components/footer'
import Content from '../components/content'

export default function Loading() {
  return (
      <div className="page">
        <Header />
        <main>
            <div className="container loading">
                <p>Catching them all...</p>
            </div>
        </main>
        <Footer />
      </div>
  )
}
