import Header from '../components/header'
import Footer from '../components/footer'
import Content from '../components/content'

export default function Error() {
  return (
      <div className="page">
        <Header />
        <main>
            <div className="container error">
                <p>Something went wrong. We weren't able to find any pokemon.</p>
            </div>
        </main>
        <Footer />
      </div>
  )
}
