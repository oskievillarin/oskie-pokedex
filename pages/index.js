import { useEffect, useState } from 'react'
import axios from 'axios'
import Header from '../components/header'
import Footer from '../components/footer'
import Content from '../components/content'

function Pokedex() {

    const [pokemons, setPokemons] = useState([])
    const [loading, setLoading] = useState(false)

    const fetchData = async () => {
        try {
            axios
                .get('https://bitbucket.org/oskievillarin/oskie-pokedex/raw/8f8400bbefd8f98de6a3c4938c6dab632a3a161d/public/pokemon.json')
                .then((res) => {
                    setPokemons(res.data.entries)
                })
            // add loading page effect
            setLoading(true)
        } catch (e) {
            console.log(e)
            // redirect here to Error page
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
      <div className="page">
        <Header />
        <Content pokemonList={pokemons} state={ loading ? true : false } />
        <Footer />
      </div>
    )
}

export default Pokedex
